jQuery(document).ready(function () {

    if (is_touch_device())
    {
        jQuery('.full-screen-scroll article').css('opacity', '1', '!important');
    }

    var ua = navigator.userAgent.toLowerCase();
    if ((ua.indexOf("safari/") !== -1 && ua.indexOf("windows") !== -1 && ua.indexOf("chrom") === -1) || is_touch_device())
    {

        jQuery("html").css('overflow', 'auto');

        jQuery(".scroll-top").click(function () {
            jQuery('html, body').animate({scrollTop: 0}, 2000);
            return false;
        });

    } else
    {
        jQuery("html, .menu-left-part, #cbp-bislideshow.scroll").niceScroll({cursorcolor: "#5B5B5B", scrollspeed: 100, mousescrollstep: 80, cursorwidth: "12px", cursorborder: "none", cursorborderradius: "0px"});

        //Scroll Top animation
        jQuery(".scroll-top").click(function () {
            jQuery("html").getNiceScroll(0).doScrollTop(0);
        });

        jQuery(".sidebar").mouseover(function () {
            jQuery(".menu-left-part").getNiceScroll().resize();
        });
    }


    //Placeholder show/hide
    jQuery('input, textarea').focus(function () {
        jQuery(this).data('placeholder', jQuery(this).attr('placeholder'));
        jQuery(this).attr('placeholder', '');
    });
    jQuery('input, textarea').blur(function () {
        jQuery(this).attr('placeholder', jQuery(this).data('placeholder'));
    });


// preload the images
    jQuery('#cbp-bislideshow.scroll').imagesLoaded(function () {
        var count = 0;
        var scrollItemWidth = jQuery('.cbp-bislideshow.scroll li').outerWidth();
        jQuery('#cbp-bislideshow.scroll').children('li').each(function () {
            var $item = jQuery(this);
            $item.css({'left': count * scrollItemWidth});
            count++;
        });
    });

    //Fix for default menu
    jQuery('.default-menu ul').addClass('main-menu sm sm-clean');



// Get the modal
var modal = document.getElementById('image_contact_modal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('image_contact');
var modalImg = document.getElementById("image_id");
var captionText = document.getElementById("caption_modal");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("image_contact_close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}


});



jQuery(window).load(function () {

    jQuery(".blog-item-holder").hover(function () {
        jQuery(".blog-item-holder").not(this).addClass('blur');
    },
            function () {
                jQuery(".blog-item-holder").removeClass('blur');
            });


//Set menu
    jQuery('.main-menu').smartmenus({
        subMenusSubOffsetX: 1,
        subMenusSubOffsetY: -8,
        markCurrentItem: true
    });


//Set each image slider
    jQuery(".image-slider").each(function () {
        var id = jQuery(this).attr('id');
        if (window[id + '_pagination'] == 'true')
        {
            var pagination_value = '.' + id + '_pagination';
        } else
        {
            var pagination_value = false;
        }

        var auto_value = window[id + '_auto'];
        if (auto_value == 'false')
        {
            auto_value = false;
        } else {
            auto_value = true;
        }

        var hover_pause = window[id + '_hover'];
        if (hover_pause == 'true')
        {
            hover_pause = 'resume';
        } else {
            hover_pause = false;
        }

        var speed_value = window[id + '_speed'];

        jQuery('#' + id).carouFredSel({
            responsive: true,
            width: 'variable',
            auto: {
                play: auto_value,
                pauseOnHover: hover_pause
            },
            pagination: pagination_value,
            scroll: {
                fx: 'crossfade',
                duration: parseFloat(speed_value)
            },
            swipe: {
                onMouse: true,
                onTouch: true
            },
            items: {
                height: 'variable'
            }
        });
    });

    jQuery('.carousel_pagination').each(function () {
        var pagination_width = jQuery(this).width();
        var windw_width = jQuery('.image-slider-wrapper').width();
        jQuery(this).css("margin-left", (windw_width - pagination_width) / 2);
    });


//Show-Hide header sidebar
    jQuery('#toggle').on("click", multiClickFunctionStop);

    //Fix for sidebar height
    jQuery("#sidebar").css('minHeight', jQuery("#content").outerHeight());

    jQuery('.doc-loader').fadeOut('fast');
});


jQuery(window).resize(function () {

//Fix for sidebar height
    jQuery("#sidebar").css('minHeight', jQuery("#content").outerHeight());

    jQuery('.menu-left-part.open').width(jQuery('.sidebar.open').width() - jQuery('.menu-right-part.open').width());

    var count = 0;
    var scrollItemWidth = jQuery('.cbp-bislideshow.scroll li').outerWidth();
    jQuery('#cbp-bislideshow.scroll').children('li').each(function () {
        var $item = jQuery(this);
        $item.css({'left': count * scrollItemWidth});
        count++;
    });

    jQuery('.carousel_pagination').each(function () {
        var pagination_width = jQuery(this).width();
        var windw_width = jQuery('.image-slider-wrapper').width();
        jQuery(this).css("margin-left", (windw_width - pagination_width) / 2);
    });

});

//------------------------------------------------------------------------
//Helper Methods -->
//------------------------------------------------------------------------


var multiClickFunctionStop = function (e) {
    e.preventDefault();
    jQuery('#toggle').off("click");
    jQuery('#toggle').toggleClass("on");
    jQuery('html, body, .sidebar, .menu-left-part, .menu-right-part').toggleClass("open");
    jQuery('.menu-left-part').width('320px');
    jQuery('.menu-left-part.open').width(jQuery('.sidebar.open').width() - jQuery('.menu-right-part.open').width());
    jQuery('#toggle').on("click", multiClickFunctionStop);
};

function is_touch_device() {
    return !!('ontouchstart' in window);
}

jQuery(window).bind("scroll", function () {
    if (jQuery(this).scrollTop() > 700) {
        jQuery('.scroll-top').fadeIn(500);
    } else
    {
        jQuery('.scroll-top').fadeOut(500);
    }
});
